// ==UserScript==
// @name     org-capture-button
// @description A button to capture with Org mode
// @copyright 2020, marcowahl (https://openuserjs.org/users/marcowahl)
// @version  0.0.2
// @author   Marco Wahl
// @licence GPL-3.0-or-later; http://www.gnu.org/licenses/gpl-3.0.txt
// @homepageURL https://gitlab.com/marcowahl
// @icon https://orgmode.org/favicon.ico
// ==/UserScript==

// What this script does
// _____________________

// Add a button to any webpage.

// Click the button to capture the current page via Org protocol using capture template X.  So precondition is to have the Org protocol up and a capture template named "X".

// Example Capture Template
// ________________________

// The capture template could look like this.

// ("X" "capture from org-protocol" entry
//       (file "~/org/captures.org")
//       "* %:description%? :webcapture:
// :PROPERTIES:
// :CREA_DATE:  %U
// :END:
// - see %:annotation %:link
// %I
// "
// :prepend t :empty-lines 1)

function capture_url() {
  const url = window.location.href
  const title = document.title
  const body = window.getSelection()
  return "org-protocol://capture?template=X" +
    "&url=" + encodeURIComponent(url) +
    "&title=" + encodeURIComponent(title) +
    "&body=" + encodeURIComponent(body)
}

const element = document.createElement("button")
element.innerHTML = "<img src=\"https://orgmode.org/favicon.ico\" alt=\"org\">"
element.style.left = "0px"
element.style.top = "0px"
element.style.position = "fixed"
element.style.minHeight = "10px"
element.style.background = "#7cb3a1"
element.style.zIndex = 2147483647
element.onclick = function () {
  window.location = capture_url()
}
document.body.appendChild(element)

// {{{ changelog :

// [2020-03-12 Thu] Raise zIndex to see the button on https://fusion.tv.

// [2020-03-01 Sun] 0.0.2 More Org'ish color and minHeight for the
// button.  (The button collapsed to almost nothing for Hacker News.)

// [2020-02-29 Sat] 0.0.1 Use the Org icon.

// [2020-02-28 Fri] 0.0.0 Start with something functional.

// }}} : changelog
